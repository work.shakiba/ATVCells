//
//  AdvancedImageCell.swift
//  Pods
//
//  Created by mohsen shakiba on 3/21/1396 AP.
//
//

import UIKit
import AdvancedTableView
import CacheImageService

public class AdvancedImageCell: Cell {

    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainImageView: AdvancedImageView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

}


public class AdvancedImageRow: Row<AdvancedImageCell> {
    
    public var placeHolder: UIImage?
    var imageAddress: String?
    var size: CGFloat = 100
    var topSpacing: CGFloat = 8
    var bottomSpacing: CGFloat = 8
    var radius: CGFloat = 0
    
    public override func setup() {
        cell?.mainImageView.url = imageAddress
        cell?.mainImageView.placeHolderImage = placeHolder
        cell?.topConstraint.constant = topSpacing
        cell?.widthConstraint.constant = size
        cell?.heightConstraint.constant = size
        cell?.mainImageView.layer.cornerRadius = radius
        borderEdge = .none
    }
    
    public override func height() -> CGFloat {
        return size + topSpacing + bottomSpacing + 16
    }
    
}
