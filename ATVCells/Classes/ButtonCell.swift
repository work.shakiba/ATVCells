//
//  ButtonCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 12/21/1395 AP.
//  Copyright © 1395 mohsen shakiba. All rights reserved.
//

import UIKit
import AdvancedTableView

public class ButtonCell: Cell {

    @IBOutlet public weak var topConstraint: NSLayoutConstraint!
    @IBOutlet public weak var label: UILabel!
    @IBOutlet public weak var container: UIView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
}


public class ButtonRow: Row<ButtonCell> {
    
    public var background: UIColor = GlobalStyles.default.accentColor()
    public var textColor: UIColor = .white
    public var font: UIFont = GlobalStyles.default.fonts.normal
    public var radius: CGFloat = 18
    
    var top: CGFloat = 8
    var bottom: CGFloat = 8
    public var text: String = ""
    
    public func size(top: CGFloat, bottom: CGFloat) {
        self.top = top
        self.bottom = bottom
    }
    
    public override func setup() {
        cell?.label.text = text
        cell?.backgroundColor = .clear
        cell?.label.textColor = .white
        cell?.label.font = font
        cell?.container.layer.cornerRadius = radius
        cell?.container.clipsToBounds = true
        cell?.container.backgroundColor = GlobalStyles.default.accentColor()
        cell?.topConstraint.constant = top
    }
    
    public override func height() -> CGFloat {
        return 36 + top + bottom
    }
    
    public override func rowHighlighted() {
        UIView.animate(withDuration: 0.5) {
            self.cell?.label.alpha = 0.2
        }
    }
    
    public override func rowUnhighlighted() {
        UIView.animate(withDuration: 0.5) {
            self.cell?.label.alpha = 1
        }
    }
    
}
