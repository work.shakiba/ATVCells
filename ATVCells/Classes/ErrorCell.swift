//
//  ErrorCell.swift
//  Pods
//
//  Created by mohsen shakiba on 3/10/1396 AP.
//
//

import UIKit
import AdvancedTableView

public class ErrorCell: Cell {
    
    @IBOutlet weak var buttonContainer: UIView!
    @IBOutlet public weak var retryButton: UILabel!
    @IBOutlet public weak var messageLabel: UILabel!
    @IBOutlet public weak var mainImageView: UIImageView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        buttonContainer.backgroundColor = UIColor(white: 1, alpha: 0.1)
        buttonContainer.layer.cornerRadius = 5
        buttonContainer.layer.borderColor = UIColor(white: 0, alpha: 0.1).cgColor
        buttonContainer.layer.borderWidth = 1
        
    }
    
}

public class ErrorRow: Row<ErrorCell> {
    
    public var message: String?
    public var retryMessage: String?
    public var image: UIImage?
    
    public override func setup() {
        cell?.messageLabel.font = GlobalStyles.default.fonts.normal
        cell?.retryButton.font = GlobalStyles.default.fonts.normal
        cell?.messageLabel.textColor = GlobalStyles.default.lightTextColor()
        cell?.retryButton.textColor = GlobalStyles.default.lightTextColor()
        cell?.messageLabel.text = message
        cell?.retryButton.text = retryMessage
        cell?.mainImageView.image = image
        borderEdge = .none
        if retryMessage == nil {
            cell?.buttonContainer.isHidden = true
        }else{
            cell?.buttonContainer.isHidden = false
        }
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onButtonPressed))
        //        cell.retryButton.addGestureRecognizer(tapGesture)
        //        cell.retryButton.isUserInteractionEnabled = true
    }
    
    public override func height() -> CGFloat {
        return 300
    }
    
}
