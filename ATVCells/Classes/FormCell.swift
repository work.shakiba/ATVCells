//
//  FormCell.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/28/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedTableView

public class FormCell: Cell {
    
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
        textView.layer.masksToBounds = false
        textView.contentInset = UIEdgeInsets.zero
        textView.textContainerInset = .zero
    }
    
}

public class FormRow: Row<FormCell>, UITextViewDelegate  {
    
    public var title: String = ""
    public var placeHolder: String?
    public var value: String?
    public var rules: [ValidationProtocol] = []
    public var error: String?
    
    override public func setup() {
        cell?.textView.isScrollEnabled = false
        cell?.titleLabel.text = title
        cell?.titleLabel.textColor = GlobalStyles.default.accentColor()
        cell?.titleLabel.font = GlobalStyles.default.fonts.small
        cell?.textView.text = value
        cell?.textView.textColor = GlobalStyles.default.textColor()
        cell?.textView.font = GlobalStyles.default.fonts.normal
        cell?.textView.isUserInteractionEnabled = false
        cell?.textView.delegate = self
        cell?.placeholderLabel.textColor = GlobalStyles.default.extraLightTextColor()
        cell?.placeholderLabel.font = GlobalStyles.default.fonts.normal
        cell?.placeholderLabel.text = placeHolder
        
        cell?.textView.textAlignment = GlobalStyles.default.textAlignent
        cell?.titleLabel.textAlignment = GlobalStyles.default.textAlignent
        
        changeStatusAccordingToStats()
        hidePlaceHolderIfNeeded()
        borderEdge = .all
    }
    
    override public func rowSelected() {
        cell?.textView.isUserInteractionEnabled = true
        cell?.textView.becomeFirstResponder()
    }
    
    override public func rowDeselected() {
        cell?.textView.isUserInteractionEnabled = false
        cell?.textView.resignFirstResponder()
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        value = textView.text
        hidePlaceHolderIfNeeded()
        updateTableViewDimentions()
    }
    
    func hidePlaceHolderIfNeeded() {
        if value?.characters.count ?? 0 > 0 {
            cell?.placeholderLabel.isHidden = true
        }else{
            cell?.placeholderLabel.isHidden = false
        }
    }
    
    public func validate() -> (value: String, error: String?) {
        for rule in rules {
            if let error = rule.error(str: self.value) {
                self.error = error
                changeStatusAccordingToStats()
                return (self.value ?? "", error)
            }
        }
        error = nil
        changeStatusAccordingToStats()
        return (value ?? "", nil)
    }
    
    private func changeStatusAccordingToStats() {
        if error != nil {
            cell?.titleLabel.text = title + " - " + error!
            cell?.titleLabel.textColor = .red
        }else{
            cell?.titleLabel.text = title
            cell?.titleLabel.textColor = GlobalStyles.default.accentColor()
        }
    }
    
    public func addValidation(rule: ValidationProtocol) {
        self.rules.append(rule)
    }
    
    override public func height() -> CGFloat {
        let label = UITextView()
        label.font = GlobalStyles.default.fonts.normal
        label.text = value
        let width = UIScreen.main.bounds.width - (24)
        let height = CGFloat.infinity
        let size = CGSize(width: width, height: height)
        return label.sizeThatFits(size).height + 20
    }
    
}


public protocol ValidationProtocol {
    
    /// returns nil if the validation succeeds and the error message if the the validation fails
    func error(str: String?) -> String?
    
}

public class StringValidationRule: ValidationProtocol {
    
    var min: Int = 0
    var max: Int = 999
    var required: Bool
    
    var maxCharError: String = "Max char not met"
    var minCharError: String = "Min char not met"
    var requiredError: String = "Field required"
    
    public init(min: Int, max: Int, required: Bool) {
        self.min = min
        self.max = max
        self.required = required
    }
    
    public func setErrors(maxErr: String, minErr: String, requiredErr: String) {
        maxCharError = maxErr
        minCharError = minErr
        requiredError = requiredErr
    }
    
    public func error(str: String?) -> String? {
        if let str = str {
            if str.characters.count < min {
                return minCharError
            }else if str.characters.count > max {
                return maxCharError
            }
            return nil
        }else{
            if required {
                return requiredError
            }
            return nil
        }
    }
    
}
