//
//  ImageCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 12/18/1395 AP.
//  Copyright © 1395 mohsen shakiba. All rights reserved.
//

import AdvancedTableView
import UIKit

public class ImageCell: Cell {
    
    @IBOutlet public weak var mainImageView: UIImageView!
    
    @IBOutlet public weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet public weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet public weak var topConstraint: NSLayoutConstraint!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
}


public class ImageRow: Row<ImageCell> {
    
    public var image: UIImage?
    var topPadding: CGFloat = 16
    var bottomPadding: CGFloat = 16
    var size: CGFloat = 100
    
    public override func setup() {
        cell?.mainImageView.image = image
        cell?.widthConstraint.constant = size
        cell?.heightConstraint.constant = size
        cell?.topConstraint.constant = topPadding
    }
    
    public func size(size: CGFloat, top: CGFloat, bottom: CGFloat) {
        topPadding = top
        bottomPadding = bottom
        self.size = size
    }
    
    public override func height() -> CGFloat {
        return size + topPadding + bottomPadding
    }
    
}
