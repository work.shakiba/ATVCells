//
//  SubtitleCell.swift
//  ShamimSalamat
//
//  Created by mohsen shakiba on 1/27/1396 AP.
//  Copyright © 1396 mohsen shakiba. All rights reserved.
//

import UIKit
import AdvancedTableView
import Font_Awesome_Swift

public class ImageTitleDetailCell: Cell {
    
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var detailLabel: UILabel!
    @IBOutlet public weak var mainImageView: UIImageView!
    @IBOutlet public weak var iconImage: UIImageView!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
}

public class ImageTitleDetailRow: Row<ImageTitleDetailCell> {
    
    public var title: String?
    public var detail: String?
    public var image: UIImage?

    public override func setup() {
        cell?.titleLabel.text = title
        cell?.detailLabel.text = detail
        cell?.titleLabel.font = GlobalStyles.default.fonts.normal
        cell?.detailLabel.font = GlobalStyles.default.fonts.normal
        cell?.titleLabel.textColor = GlobalStyles.default.accentColor()
        cell?.detailLabel.textColor = GlobalStyles.default.lightTextColor()
        cell?.iconImage.setFAIconWithName(icon: .FAAngleLeft, textColor: UIColor(white: 0, alpha: 0.3) )
        borderEdge = .none
    }
    
    public override func height() -> CGFloat {
        return 60 + 16
    }
}
