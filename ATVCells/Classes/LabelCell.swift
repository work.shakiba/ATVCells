//
//  LabelCell.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedTableView

public class LabelCell: Cell {

    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

}

public class LabelRow: Row<LabelCell> {
    
    public var textAlignment: NSTextAlignment = .right
    public var textColor: UIColor = .black
    public var numberOfLines: Int = 1
    public var font: UIFont = GlobalStyles.default.fonts.normal
    public var text: String?
    var topPadding: CGFloat = 8
    var bottomPadding: CGFloat = 8
    
    override public func setup() {
        cell?.label.text = text
        cell?.label.textColor = textColor
        cell?.label.textAlignment = textAlignment
        cell?.label.numberOfLines = numberOfLines
        cell?.label.font = font
        cell?.topContraint.constant = topPadding
        cell?.bottomContraint.constant = bottomPadding
        borderEdge = .none
    }
    
    public func size(top: CGFloat, bottom: CGFloat) {
        self.topPadding = top
        self.bottomPadding = bottom
    }
    
    override public func height() -> CGFloat {
        let label = UILabel()
        label.numberOfLines = numberOfLines
        label.font = font
        label.text = text
        let width = UIScreen.main.bounds.width - (2 * 16)
        let height = CGFloat.infinity
        let size = CGSize(width: width, height: height)
        return label.sizeThatFits(size).height + topPadding + bottomPadding
    }
    
}

