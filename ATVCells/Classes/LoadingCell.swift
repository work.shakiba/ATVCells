//
//  LoadingCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 2/5/1396 AP.
//  Copyright © 1396 mohsen shakiba. All rights reserved.
//

import UIKit
import AdvancedTableView
import NVActivityIndicatorView

public class LoadingCell: Cell {

    @IBOutlet public weak var messageLabel: UILabel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        let indicator = NVActivityIndicatorView(frame: CGRect.zero, type: NVActivityIndicatorType.ballBeat, color: GlobalStyles.default.accentColor(), padding: 0)
        indicator.frame = CGRect(x: 0, y: 25, width: UIScreen.main.bounds.width, height: 60)
        indicator.startAnimating()
        self.contentView.addSubview(indicator)
    }

}

public class LoadingRow: Row<LoadingCell> {
    
    public var text: String?
    
    public override func setup() {
        cell?.messageLabel.font = GlobalStyles.default.fonts.normal
        cell?.messageLabel.textColor = GlobalStyles.default.lightTextColor()
        cell?.messageLabel.text = text
        borderEdge = .none
    }
    
    public override func height() -> CGFloat {
        return  60 + 20 + 16 + 50
    }
    
}
