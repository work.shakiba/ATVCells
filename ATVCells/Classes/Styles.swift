//
//  Styles.swift
//  Pods
//
//  Created by mohsen shakiba on 3/28/1396 AP.
//
//

import Foundation

open class GlobalStyles {
    
    static public var `default` = GlobalStyles()
    
    public init() {
        
    }
    
    public var textAlignent: NSTextAlignment {
        get { return defaultTextAlignment() }
    }
    
    open func defaultFont(of size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    
    open func accentColor() -> UIColor {
        return .black
    }
    
    open func textColor() -> UIColor {
        return .black
    }
    
    open func lightTextColor() -> UIColor {
        return .black
    }
    
    open func extraLightTextColor() -> UIColor {
        return .black
    }
    
    open func borderColor() -> UIColor {
        return .black
    }
    
    public var fonts = FontStyles()
    
    open func backgroundLight() -> UIColor {
        return UIColor(white: 0.95, alpha: 1)
    }
    
    open func defaultTextAlignment() -> NSTextAlignment {
        return .right
    }
    
}

public struct FontStyles {
    
    
    public var small: UIFont {
        get { return GlobalStyles.default.defaultFont(of: 13) }
    }
    
    public var normal: UIFont {
        get { return GlobalStyles.default.defaultFont(of: 16) }
    }
    
    public var large: UIFont {
        get { return GlobalStyles.default.defaultFont(of: 19) }
    }
    
}
