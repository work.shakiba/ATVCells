//
//  TitleDetailCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 1/3/1396 AP.
//  Copyright © 1396 mohsen shakiba. All rights reserved.
//

import UIKit
import AdvancedTableView
import Font_Awesome_Swift

public class TitleDetailCell: Cell {
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleRightConstraint: NSLayoutConstraint!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var detailLabel: UILabel!
    @IBOutlet public weak var detailImageView: UIImageView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
        detailImageView.setFAIconWithName(icon: .FAAngleLeft, textColor: GlobalStyles.default.extraLightTextColor())
        
        titleLabel.textColor = GlobalStyles.default.textColor()
        detailLabel.textColor = GlobalStyles.default.lightTextColor()
        
        titleLabel.font = GlobalStyles.default.fonts.normal
        detailLabel.font = GlobalStyles.default.fonts.small
        
        titleLabel.textAlignment = .right
        detailLabel.textAlignment = .left
        
    }
    
}

public class TitleDetailRow: Row<TitleDetailCell> {
    
    public var title: String?
    public var detail: String?
    public var image: UIImage?
    
    var size: CGFloat = 55
    
    public override func setup() {
        
        cell?.titleLabel.text = title
        cell?.detailLabel.text = detail
        
        if let image = self.image {
            cell?.mainImageView.image = image
            cell?.mainImageView.isHidden = false
            cell?.titleRightConstraint.constant = -size
        }else{
            cell?.mainImageView.isHidden = true
            cell?.titleRightConstraint.constant = -8
        }
        
        borderEdge = .all
        
    }
    
    public func size(_ height: CGFloat) {
        self.size = height
    }
    
    public override func rowHighlighted() {
        cell?.backgroundColor = GlobalStyles.default.backgroundLight()
    }
    
    public override func rowUnhighlighted() {
        cell?.backgroundColor = .white
    }
    
    public override func height() -> CGFloat {
        return size
    }
    
}
