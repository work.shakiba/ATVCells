//
//  HeaderDetailCell.swift
//  Entekhabat
//
//  Created by mohsen shakiba on 1/16/1396 AP.
//  Copyright © 1396 mohsen shakiba. All rights reserved.
//

import UIKit
import AdvancedTableView

public class TitleDetailExpandedCell: Cell {
    
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var detailLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
    }
    
}

public class TitleDetailExpandedRow: Row<TitleDetailExpandedCell> {
    
    public var title: String?
    public var detail: String?
    
    public var titleColor: UIColor = GlobalStyles.default.accentColor()
    public var detailColor: UIColor = GlobalStyles.default.textColor()
    
    public var titleFont: UIFont = GlobalStyles.default.fonts.small
    public var detailFont: UIFont = GlobalStyles.default.fonts.normal
    
    public override func setup() {
        cell?.titleLabel.text = title
        cell?.detailLabel.text = detail
        cell?.titleLabel.textColor = titleColor
        cell?.detailLabel.textColor = detailColor
        cell?.titleLabel.font = titleFont
        cell?.detailLabel.font = detailFont
        cell?.titleLabel.textAlignment = GlobalStyles.default.textAlignent
        cell?.detailLabel.textAlignment = GlobalStyles.default.textAlignent
        borderEdge = .all

    }
    
    public override func rowHighlighted() {
        cell?.backgroundColor = GlobalStyles.default.backgroundLight()
    }
    
    public override func rowUnhighlighted() {
        cell?.backgroundColor = .white
    }
    
    public override func height() -> CGFloat {
        let label = UILabel()
        label.numberOfLines = 99
        label.font = GlobalStyles.default.fonts.normal
        var size = UIScreen.main.bounds.size
        size.width -= 32
        let labelHeight = label.sizeThatFits(size).height
        return labelHeight
    }
    
}
