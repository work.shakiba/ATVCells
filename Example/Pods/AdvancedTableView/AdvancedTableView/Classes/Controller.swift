//
//  ListViewController.swift
//  Pods
//
//  Created by mohsen shakiba on 12/12/1395 AP.
//
//

import Foundation
import UIKit
import RxSwift

/**
 * the class used by view controllers to implement the list view
 **/

open class AdvancedTableViewController: CoreController {
    
    
    /*************Properties**************/

    public var tableView: UITableView {
        get { return super._tableView }
    }
    
    
    // will prevent the list view froms disposing if the controller's viewDidDisappear is called
    public var preventDispose = false {
        didSet {
            preventControllerFromClearingReferences = preventDispose
        }
    }
    
    // if animations are enabled
    public var animations: Bool = true {
        didSet {
            UIView.setAnimationsEnabled(animations)
        }
    }
    
    /// constraints used for table view
    public var constraints: ListViewConstraints {
        get { return tableViewConstraints }
    }
    
    /// anything that needs to be disposed
    open override func prepareForDispose() {
    }
    
    
    /*************Common**************/

    
    /// view did load
    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// override to change the behaviour of table view when the keyboard is vaibile
    open override func keyboard(visible: Bool, rect: CGRect) {
        if visible {
            tableView.contentInset.bottom +=  rect.height + 50
        }else{
            tableView.contentInset.bottom -= rect.height + 50
        }
    }
    
    
    /// called when table view is reached end of section
    open override func reachedEnd(of section: Section) {
        
    }
    
    /// begin updates for changes
    public func beginUpdates() {
        if !updateInstantly {
            return
        }
        tableView.beginUpdates()
        self.updateInstantly = false
    }
    
    /// end updates and apply all changes done while begin updates is called
    public func endUpdates(_ withAnimations: Bool = true) {
        self.updateInstantly = true
        let lastAnimationStatus = self.animations
        self.animations = withAnimations
        super.endUpdatesIfNeeded()
        self.animations = lastAnimationStatus
    }
    
    
    /*************Section**************/
    
    
    /// create new section provider and pass in closure
    public func section(_ configHandler: ((Section) -> Void)? = nil ) -> Section {
        let id = String.randomString(length: 5)
        let listViewSection = Section(id: id, controller: self)
        listViewSection.configHandler = configHandler
        return listViewSection
    }
    
    /// returns section provider by id
    public override func sectionBy(id: String) -> Section? {
        return super.sectionBy(id: id)
    }
    
    /// remove and dispose the section by id
    public func removeSectionBy(id: String) {
        deleteSection(id)
    }
    
    /// reload section by id
    public func reloadSectionBy(id: String) {
        if let sectionIndex = sectionIndexBy(id) {
            let indexSet = NSIndexSet(index: sectionIndex) as IndexSet
            self.tableView.reloadSections(indexSet, with: .fade)
        }
    }
    
    /*************Cell**************/

    
//    // create new section and insert new row
//    public func row<RowT, Cell>(_ type: RowT.Type) -> RowTransferObject<RowT, Cell> where RowT: Row<Cell>, Cell: UITableViewCell, Cell: UITableViewCell {
//        var row: RowTransferObject<RowT, Cell>!
//        section { (s) in
//            row = s.row(type)
//            }
//            .finalize()
//        return row
//    }
    
    /// find row by id
    public func rowBy<T>(id: String) -> T? where T: BaseRow  {
        for s in visibleSections {
            if let cell = s.rowBy(id: id) {
                return cell as? T
            }
        }
        return nil
    }
    
    /// reload row by the given id and update according to the given the handler
    public func reloadRowBy<T>(id: String, _ handler: ((T) -> Void)?) where T: BaseRow {
        for section in visibleSections {
            section.reloadRowBy(id: id, handler)
        }
    }
    
    
    /// remove cell by
    public func removeRowBy(id: String) {
        if let row = self.rowBy(id: id) {
            row.section?.removeRowBy(id: id)
        }
    }

    
}


open class CoreController: UIViewController {
    
    var id = String.randomString(length: 10)
    
    var preventControllerFromClearingReferences = false
    
    var _tableView: UITableView!
    
    let repository = Repository<Section>()
    var needCacheReset = false
    var fadeIn = false
    var cellIsFirstResponder = false
    
    var visibleSections: [Section] {
        get {
            return repository.visibleItems
        }
    }
    
    var allSections: [Section] {
        get {
            return repository.allItems
        }
    }
    
    internal var isKeyboardVisibile: Bool = false
    
    let tableViewConstraints = ListViewConstraints()
    
    var updateInstantly = true
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        listenToKeyboardChange()
        self.setup()
    }
    
    
    open func setup() {
        
    }
    
    
    func setupTableView() {
        self._tableView = UITableView(frame: self.view.bounds, style: .grouped)
        _tableView.delegate = self
        _tableView.translatesAutoresizingMaskIntoConstraints = false
        _tableView.keyboardDismissMode = .onDrag
        _tableView.separatorStyle = .none
        _tableView.dataSource = self
        self.view.addSubview(_tableView)
        addConstraints()
    }
    
    func addConstraints() {
        tableViewConstraints.top = NSLayoutConstraint(item: _tableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: topLayoutGuide, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        tableViewConstraints.bottom = NSLayoutConstraint(item: _tableView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: bottomLayoutGuide, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        tableViewConstraints.left = NSLayoutConstraint(item: _tableView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        tableViewConstraints.right = NSLayoutConstraint(item: _tableView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        view.addConstraints([tableViewConstraints.top, tableViewConstraints.bottom, tableViewConstraints.left, tableViewConstraints.right])
    }
    
    func listenToKeyboardChange() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        if preventControllerFromClearingReferences { return }
        super.viewDidDisappear(animated)
        var found = false
        self.navigationController?.viewControllers.forEach({ (controller) in
            if let controller = controller as? CoreController {
                if controller.id == self.id {
                    found = true
                }
            }
        })
        if !found {
            dispose()
        }
    }
    
    func prepareForDispose() {
        
    }
    
    func dispose() {
        prepareForDispose()
        for section in allSections {
            section.dispose()
        }
    }
    
    func keyboardWillShow(_ notification: Notification){
        if isKeyboardVisibile {
            return
        }
        isKeyboardVisibile = true
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboard(visible: self.isKeyboardVisibile, rect: keyboardSize)
        }
    }
    
    
    func keyboardWillHide(_ notification: Notification){
        if !isKeyboardVisibile {
            return
        }
        isKeyboardVisibile = false
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboard(visible: self.isKeyboardVisibile, rect: keyboardSize)
        }
    }
    
    func keyboard(visible: Bool, rect: CGRect) {
        
    }
    
    func didReachBottom(of section: Section) {
        DispatchQueue.main.async {
            self.reachedEnd(of: section)
        }
    }
    
    func reachedEnd(of section: Section) {
        
    }
    
    
    func rowIndexPathBy(id: String) -> IndexPath? {
        for (i, s) in visibleSections.enumerated() {
            if let cellIndex = s.rowIndexBy(id: id) {
                return IndexPath(row: cellIndex, section: i)
            }
        }
        return nil
    }
    
    // returns index of section by id
    func sectionIndexBy(_ id: String) -> Int? {
        for (i, s) in visibleSections.enumerated() {
            if s.id == id {
                return i
            }
        }
        return nil
    }
    
    func sectionBy(id: String) -> Section? {
        return allSections.first(where: { $0.id == id })
    }
    
    func sectionByIndex(_ sectionIndex: Int) -> Section {
        let section = visibleSections[sectionIndex]
        return section
    }
    
    func deleteSection(_ id: String) {
        self.beginUpdatesIfNeeded()
        if let section = self.sectionBy(id: id) {
            section.nextOperation = .changeToRemove
        }
        self.endUpdatesIfNeeded()
    }
    
    func commitSection(section: Section, at: Int? = nil) {
        self.beginUpdatesIfNeeded()
        self.repository.insert(row: section, at: at)
        section.configIfNeeded()
        self.endUpdatesIfNeeded()
    }
    
    func beginUpdatesIfNeeded() {
        if updateInstantly {
            _tableView.beginUpdates()
        }
    }
    
    func endUpdatesIfNeeded() {
        if updateInstantly {            
            logEvent("sections before update \(self.repository.visibleItems.count)")
            
            let sectionsToRemove = repository.itemsToDelete()
            for section in sectionsToRemove {
                if let index = section.nextOperationIndex {
                    logEvent("section deleted by id: \(section.id) and index: \(index)")
                    let indexSet = NSIndexSet(index: index)
                    _tableView.deleteSections(indexSet as IndexSet, with: .fade)
                }
                section.index = nil
            }
            
            let sectionsToAdd = repository.itemsToInsert()
            for section in sectionsToAdd {
                if let index = section.nextOperationIndex {
                    logEvent("section inserted by id: \(section.id) and index: \(index)")
                    let indexSet = NSIndexSet(index: index)
                    _tableView.insertSections(indexSet as IndexSet, with: .fade)
                }
            }
            
            repository.invalidateSnapshot()
            
            for section in allSections {
                section.index = repository.indexInNextSnapshot(id: section.id)
                let indexInNext = repository.indexInNextSnapshot(id: section.id)
                let indexInCurrent = repository.indexInCurrentSnapshot(id: section.id)
                section.updateIfNeeded(index: indexInCurrent, next: indexInNext)
            }
            
            repository.invalidateCache(log: true)
            
            logEvent("sections after update \(self.repository.visibleItems.count)")
            self.fadeIn = true
            _tableView.endUpdates()
        }
    }
    
}

extension CoreController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return visibleSections.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sectionByIndex(indexPath.section)
        let reuseIdentifier = section.cellReuseIdentifierBy(row: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier!)
        let row = section.rowBy(index: indexPath.row)
        row.innerCell = cell
        if let advancedCell = cell as? Cell {
            advancedCell.row = row
        }
        row.onDequeue()
        return cell!
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let lvsection = sectionByIndex(section)
        logEvent("expected number of rows in section by index \(section) is \(lvsection.visibleRows.count)")
        return lvsection.visibleRows.count
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = sectionByIndex(indexPath.section)
        let height = section.rowHeightBy(row: indexPath.row)
        return height
    }
    
}

extension CoreController: UITableViewDelegate {
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        let row = section.rowBy(index: indexPath.row)
        row.onClick()
    }
    
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        let row = section.rowBy(index: indexPath.row)
        row.rowDeselected()
    }
    
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        let row = section.rowBy(index: indexPath.row)
        row.rowHighlighted()
    }
    
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        let row = section.rowBy(index: indexPath.row)
        row.rowUnhighlighted()
    }
    
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let section = sectionByIndex(indexPath.section)
        let row = section.rowBy(index: indexPath.row)
        if fadeIn {
            let lastAnimationState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(true)
            cell.alpha = 0
            UIView.animate(withDuration: 0.25, animations: {
                cell.alpha = 1
            }, completion: { (_) in
                self.fadeIn = false
            })
            UIView.setAnimationsEnabled(lastAnimationState)
            
        }
        row.willDispaly(indexPath: indexPath)
        if section.didReachBottom(row: indexPath.row) {
            self.didReachBottom(of: section)
        }
    }
    
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = sectionByIndex(section)
        return section.getHeaderView()
    }
    
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let section = sectionByIndex(section)
        return section.getFooterView()
    }
    
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = sectionByIndex(section)
        return section.getHeaderHeight()
    }
    
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let section = sectionByIndex(section)
        return section.getFooterHeight()
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let section = sectionByIndex(indexPath.section)
        let corerow = section.rowBy(index: indexPath.row)
        return corerow.editingActions
    }
    
}


public class ListViewConstraints {
    public var top: NSLayoutConstraint!
    public var bottom: NSLayoutConstraint!
    public var left: NSLayoutConstraint!
    public var right: NSLayoutConstraint!
}


//public protocol AdvancedTableViewProtocol {
//    
//    var viewController: UIViewController { get }
//    var tableView: UITableView { get }
//    
//    func setup()
//    
//    func initialize()
//    
//}
//
//public extension AdvancedTableViewProtocol {
//    
//
//    func initialize() {
//        viewController.view.addSubview(tableView)
//        tableView.delegate = self
//        tableView.dataSource = self
//        let frame = self.advancedTableView(frameFor: tableView)
//        let resizingMasks = self.advancedTableView(autoresizingFor: tableView)
//        tableView.frame = frame
//        tableView.autoresizingMask = resizingMasks
//    }
//    
//    
//    func advancedTableView(frameFor tableView: UITableView) -> CGRect {
//        return viewController.view.bounds
//    }
//    
//    func advancedTableView(autoresizingFor tableView: UITableView) -> UIViewAutoresizing {
//        return [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleHeight]
//    }
//    
//}
//
//extension AdvancedTableViewProtocol: UITableViewDataSource {
//    
//    
//    
//}
//
//extension AdvancedTableViewProtocol: UITableViewDelegate {
//    
//}

