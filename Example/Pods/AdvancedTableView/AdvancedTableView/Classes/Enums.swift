//
//  CellEvents.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

import Foundation

public enum CellEvents {
    case dequeued
    case appeared
    case selected
    case deSelected
    case highlighted
    case unHighlighted
}

enum NextOperation {
    case changeToHidden
    case changoToVisible
    case hidden
    case visible
    case changeToRemove
    case changeToCommit
}

public enum BorderEdge {
    
    case none
    case bottom
    case all
    
}


public enum KeyboardStatus {
    
    case visible
    case hidden
    
}
