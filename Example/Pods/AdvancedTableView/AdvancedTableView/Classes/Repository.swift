//
//  CellProviderRepository.swift
//  Pods
//
//  Created by mohsen shakiba on 2/7/1396 AP.
//
//

import Foundation


protocol VisibleItem: class {
    
    var id: String! { get set }
    var nextOperation: NextOperation { get set }
    var nextOperationIndex: Int? { get set }
    func isVisible() -> Bool
    
}

class Repository<T> where T: VisibleItem {
    
    internal var items: [T] = []
    internal var snapShot: [T] = []
    internal var nextSnapshot: [T]?
    var visibleItems: [T] {
        get { return snapShot }
    }
    
    var allItems: [T] {
        get { return items }
    }
    
    // insert or append new provider to repository and get the index in the snapshot
    func insert(row: T, at: Int?) {
        if let at = at {
            self.items.insert(row, at: at)
        }else{
            self.items.append(row)
            if row.isVisible() {
                self.snapShot.append(row)
            }
        }
    }
    
    // clear all the repository
    func clear() {
        for row in allItems {
            row.nextOperation = .changeToRemove
        }
    }
    
    // invalidate the snapshot
    func invalidateCache(log: Bool) {
        snapShot = rebuildSnapshot()
        nextSnapshot = nil
    }
    
    func invalidateSnapshot() {
        self.nextSnapshot = self.rebuildSnapshot()
    }
    
    // recreate the snapshot
    private func rebuildSnapshot() -> [T] {
        let nextSnapshot = items.filter({ (provider) -> Bool in
            let isVisible = provider.isVisible()
            return isVisible
        })
        self.nextSnapshot = nextSnapshot
        return nextSnapshot
    }
    
    func itemsToDelete() -> [T] {
        let rows = visibleItems.filter({$0.nextOperation == .changeToRemove || $0.nextOperation == .changeToHidden })
        for row in items {
            if row.nextOperation == .changeToRemove {
                if let index = items.index(where: {$0.id == row.id}) {
                    items.remove(at: index)
                }
            }
        }
        for row in rows {
            row.nextOperation = .hidden
            row.nextOperationIndex = indexInCurrentSnapshot(id: row.id)
        }
        return rows
    }
    
    func itemsToInsert() -> [T] {
        let rows = allItems.filter({$0.nextOperation == .changoToVisible || $0.nextOperation == .changeToCommit})
        
        if rows.contains(where: {$0.nextOperation == .changeToCommit }) {
            for row in rows {
                row.nextOperation = .visible
            }
            self.nextSnapshot = rebuildSnapshot()
        }
        
        for row in rows {
            row.nextOperation = .visible
            row.nextOperationIndex = indexInNextSnapshot(id: row.id)
        }
        return rows
    }
    
    // get index of provider in current snapshot
    func indexInCurrentSnapshot(id: String) -> Int? {
        return snapShot.index(where: {$0.id == id})
    }
    
    // get index of provider in next built snapshot
    func indexInNextSnapshot(id: String) -> Int? {
        if let nextSnapshot = nextSnapshot {
            return nextSnapshot.index(where: {$0.id == id})
        }else {
            return rebuildSnapshot().index(where: {$0.id == id})
        }
    }
    
}

