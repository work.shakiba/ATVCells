//
//  Row.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

import Foundation



//open class Row<CELL>: BaseRow where CELL: Cell {
//    
//    var actions: [UITableViewRowAction] = [] {
//        didSet {
//            self.editingActions = actions
//        }
//    }
//
//    public var cell: CELL? {
//        get { return self.innerCell as? CELL }
//    }
//    
//    open override func setup() {
//        
//    }
//    
//    open override func height() -> CGFloat {
//        fatalError("height not spec")
//    }
//    
//    public func onClick(handler: @escaping () -> Void) {
//        self.clickHandler = handler
//    }
//    
//    public func onEvent(handler: @escaping (String?, Any?) -> Void) {
//        self.eventHandler = handler
//    }
//
//}


//open class RowTransferObject<RowType, CellType> where CellType: UITableViewCell, RowType: Row<CellType> {
//    
//    var row: BaseRow
//    
//    public init(_ row: BaseRow) {
//        self.row = row
//    }
//    
//    public func setup(_ handler: @escaping (RowType) -> Void) -> RowTransferObject<RowType, CellType> {
//        let newHandler: (RowProtocol) -> Void = { row in
//            handler(row as! RowType)
//        }
//        row.prepareHandler = newHandler
//        return self
//    }
//    
//    public func click(_ handler: @escaping () -> Void) -> RowTransferObject<RowType, CellType> {
//        row.clickHandler = handler
//        return self
//    }
//    
//    public func update(_ handler: @escaping (RowType) -> Void) -> RowTransferObject<RowType, CellType> {
//        let newHandler: (RowProtocol) -> Void = { row in
//            handler(row as! RowType)
//        }
//        row.updateHandler = newHandler
//        return self
//    }
//    
//    public func event(_ handler: @escaping (String, Any?) -> Void) -> RowTransferObject<RowType, CellType> {
//        row.eventHandler = handler
//        return self
//    }
//    
//    public func editingActions(_ handler: @escaping () -> [UITableViewRowAction]) -> RowTransferObject<RowType, CellType> {
//        let actions = handler()
//        self.row.editingActions = actions
//        return self
//    }
//    
//    
//    
//    //    public func model(_ handler: () -> ModelType) -> RowTransferObject<RowType, CellType>  {
//    //        row.innerModel = handler()
//    //        return self
//    //    }
//    
//    public func commit() {
//        row.section?.commitRow(row, at: nil, asHidden: false)
//    }
//    
//    public func commit(_ id: String? = nil, _ at: Int? = nil, asHidden: Bool = false) {
//        row.id = id ?? self.row.id
//        row.section?.commitRow(row, at: at, asHidden: asHidden)
//    }
//    
//}
//

open class Row<CellType>: BaseRow where CellType: UITableViewCell {
    
    //    public var model: ModelType { get { return innerModel as! ModelType } set { self.innerModel = newValue } }
    public var cell: CellType? { get { return innerCell as? CellType } }
    
    open override func height() -> CGFloat {
        fatalError("Height not specified by row")
    }
    
    open override func setup() {
        fatalError("Setup not defined")
    }
    
    public override func commit(_ section: Section) {
        
        let bundle = Bundle(for: CellType.classForCoder())
        let nib = UINib.fromType(type: CellType.self, bundle: bundle)
        let nibName = UINib.name(type: CellType.self)
        section.controller?.tableView.register(nib, forCellReuseIdentifier: nibName)
        
        self.section = section
        self.id = id ?? String.randomString(length: 10)
        self.reuseIdentifier = nibName
        section.accept(row: self, at: self.index, asHidden: self.hidden ?? false)
    }
    
    public override func commit(_ controller: AdvancedTableViewController) {
        let bundle = Bundle(for: CellType.classForCoder())
        let nib = UINib.fromType(type: CellType.self, bundle: bundle)
        let nibName = UINib.name(type: CellType.self)
        controller.tableView.register(nib, forCellReuseIdentifier: nibName)
        
        self.section = section
        self.id = id ?? String.randomString(length: 10)
        self.reuseIdentifier = nibName
        section?.accept(row: self, at: self.index, asHidden: self.hidden ?? false)
    }

}



open class BaseRow : NSObject, RowProtocol, VisibleItem {
    
    public var id: String!
    var reuseIdentifier: String!
    var nextOperation: NextOperation = .changeToCommit
    var nextOperationIndex: Int?
    var innerModel: Any?
    var innerCell: UITableViewCell?
    var clickHandler: (() -> Void)?
    var prepareHandler: ((RowProtocol) -> Void)?
    var updateHandler: ((RowProtocol) -> Void)?
    var eventHandler: ((String, Any?) -> Void)?
    var nextUpdateHandler: ((RowProtocol) -> Void)?
    var editingActions: [UITableViewRowAction] = []
    public var borderEdge: BorderEdge = .bottom
    
    weak var section: Section?
    
    var index: Int?
    var hidden: Bool?
    
    var isSetupCalled = false
    
    public func show() {
        if isVisible() { return }
        self.section?.beginUpdatesIfNeeded()
        nextOperation = .changoToVisible
        self.section?.endUpdatesIfNeeded()
    }
    
    public func hide() {
        if !isVisible() { return }
        self.section?.beginUpdatesIfNeeded()
        nextOperation = .changeToHidden
        self.section?.endUpdatesIfNeeded()
    }
    
    public func updateTableViewDimentions() {
        UIView.setAnimationsEnabled(false)
        self.section?.controller?.beginUpdatesIfNeeded()
        self.section?.controller?.endUpdatesIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    public func remove() {
        self.section?.beginUpdatesIfNeeded()
        nextOperation = .changeToRemove
        self.section?.endUpdatesIfNeeded()
    }
    
    public func reload() {
        self.section?.reloadRowBy(id: self.id)
    }
    
    open func height() -> CGFloat {
        fatalError("Height not specified by row")
    }
    
    open func setup() {
        fatalError("Setup not defined")
    }
    
    public func dispose() {
        self.clickHandler = nil
        self.updateHandler = nil
        self.nextUpdateHandler = nil
        self.eventHandler = nil
    }
    
    func isVisible() -> Bool {
        if nextOperation == .visible || nextOperation == .changeToCommit || nextOperation == .changoToVisible {
            return true
        }
        return false
    }
    
    func onDequeue() {
        if let prepareHandler = self.prepareHandler {
            prepareHandler(self)
            self.prepareHandler = nil
        }
        // call handler
        if let updateHandler = self.updateHandler {
            updateHandler(self)
        }
        if let nextUpdateHandler = self.nextUpdateHandler {
            nextUpdateHandler(self)
            self.nextUpdateHandler = nil
        }
        setup()
    }
    
    func willDispaly(indexPath: IndexPath) {
        self.updateBorder(isFirst: indexPath.row == 0)
        if !(section?.controller?.cellIsFirstResponder ?? true) {
            section?.controller?.cellIsFirstResponder = self.keyboardVisible()
        }
    }
    
    func onClick() {
        if let onClickHandler = self.clickHandler {
            onClickHandler()
        }
        rowSelected()
    }
    
    func updateBorder(isFirst: Bool) {
        guard let cell = innerCell as? Cell else { return }
        let edge = self.borderEdge
        let borderColor = cell.borderColor()
        let inset = cell.borderInset()
        
        if edge == .all && isFirst {
            self.innerCell?.borderIfNeeded(.top, color: borderColor, inset: inset)
        }
        if edge == .bottom || edge == .all {
            self.innerCell?.borderIfNeeded(.bottom, color: borderColor, inset: inset)
        }
        
        if edge == .none {
            self.innerCell?.subviews.forEach({view in
                if view.tag == 1111 {
                    view.removeFromSuperview()
                }
            })
        }
    }
    
    var prevBackgroundColor: UIColor?
    open func rowHighlighted() {
        guard let cell = innerCell as? Cell else {
            return
        }
        prevBackgroundColor = cell.backgroundColor
        innerCell?.backgroundColor = cell.highlightColor()
    }
    
    open func rowUnhighlighted() {
        innerCell?.backgroundColor = prevBackgroundColor
    }
    
    open func rowSelected() {
        
    }
    
    open func rowDeselected() {
        
    }
    
    open func keyboardVisible() -> Bool {
        return false
    }
    
    public func emit(event: String, value: Any?) {
        if let handler = self.eventHandler {
            handler(event, value)
        }
    }
    
    public func commit(_ section: Section) {
        fatalError("commit not implemented")
    }
    
    public func commit(_ controller: AdvancedTableViewController) {
        fatalError("commit not implemented")
    }
}


public protocol RowProtocol: class {
    
    func commit(_ section: Section)
    func commit(_ controller: AdvancedTableViewController)
    func remove()
    func reload()
    func show()
    func hide()
    func height() -> CGFloat
    func setup()
}


public extension RowProtocol {
    
    
    public func click(_ handler: @escaping () -> Void) -> Self {
        let row = self as! BaseRow
        row.clickHandler = handler
        return self
    }
    
    public func event(_ handler: @escaping (String?, Any?) -> Void) -> Self {
        let row = self as! BaseRow
        row.eventHandler = handler
        return self
    }
    
    public func at(_ index: Int) -> Self  {
        let row = self as! BaseRow
        row.index = index
        return self
    }
    
    public func asHidden(_ hidden: Bool) -> Self  {
        let row = self as! BaseRow
        row.hidden = hidden
        return self
    }
    
    public func id(_ id: String) -> Self  {
        let row = self as! BaseRow
        row.id = id
        return self
    }
    
    public func onDisplay(_ handler: @escaping (Self) -> Void) -> Self {
        let row = self as! BaseRow
        row.updateHandler = { rowProt in
            handler(row as! Self)
        }
        return self
    }
    
    public func customize(_ handler: (Self) -> Void) -> Self {
        handler(self)
        return self
    }
    
}

