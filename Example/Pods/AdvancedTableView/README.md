# AdvancedTableView

[![CI Status](http://img.shields.io/travis/mohsenShakiba/AdvancedTableView.svg?style=flat)](https://travis-ci.org/mohsenShakiba/AdvancedTableView)
[![Version](https://img.shields.io/cocoapods/v/AdvancedTableView.svg?style=flat)](http://cocoapods.org/pods/AdvancedTableView)
[![License](https://img.shields.io/cocoapods/l/AdvancedTableView.svg?style=flat)](http://cocoapods.org/pods/AdvancedTableView)
[![Platform](https://img.shields.io/cocoapods/p/AdvancedTableView.svg?style=flat)](http://cocoapods.org/pods/AdvancedTableView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdvancedTableView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "AdvancedTableView"
```

## Author

mohsenShakiba, work.shakiba@gmail.com

## License

AdvancedTableView is available under the MIT license. See the LICENSE file for more info.
