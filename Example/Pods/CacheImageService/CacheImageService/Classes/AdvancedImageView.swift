//
//  AdvancedImageView.swift
//  Pods
//
//  Created by mohsen shakiba on 2/23/1396 AP.
//
//

import UIKit
import CollieGallery

public class AdvancedImageView: UIView {
    
    // image view
    var imageView: UIImageView!
    // url for current image
    var urlString: String?
    // id for subscription of image
    var id: String?
    // hash id for checking validity of proess in multi thread enviroment
    var hashId: Int = 0
    // view controller for presenting the image
    var viewController: UIViewController?
    
    // content mode for image
    public var contentModed: UIViewContentMode {
        set {
            self.imageView.contentMode = newValue
        }
        get {
            return self.imageView.contentMode
        }
    }
    
    // url for the current image
    public var url: String? {
        set {
            if let url = newValue {
                self.loadUri(url)
            }
        }
        get {
            return urlString
        }
    }
    
    // current image
    public var image: UIImage? {
        set {
            if let image = newValue {
                onImageSet(image)
            }
        }
        get {
            if let uri = self.url, let data = CacheService.default.getFile(uri) {
                return UIImage(data: data)
            }
            return nil
        }
    }
    
    // place holder for the current image
    public var placeHolderImage: UIImage? {
        set {
            self.imageView.image = newValue
        }
        get {
            return self.imageView.image
        }
    }
    
    // default init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    // default init
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        config()
    }
    
    // assign the controller for presenting the image
    public func showGalleryOnTap(_ viewController: UIViewController) {
        self.viewController = viewController
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
        imageView.addGestureRecognizer(tap)
    }
    
    // config the view
    internal func config() {
        imageView = UIImageView(frame: frame)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        let constraintTop = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let constraintRight = NSLayoutConstraint(item: imageView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        let constraintBottom = NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let constraintLeft = NSLayoutConstraint(item: imageView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([constraintTop, constraintRight,constraintBottom,constraintLeft])
    }
    
    // load the uri
    internal func loadUri(_ uri: String) {
        self.imageView.image = nil
        self.urlString = uri
        // cancel subscription
        self.cancelSubscription()
        // set new id
        self.id = String.randomString(length: 10)
        self.hashId = uri.hash
        // check if data exists
        if let data = CacheService.default.getFile(uri) {
            self.onDataSet(data, uri.hash)
        }else{
            // prepare view
            prepareForDownload()
            // download the image
            CacheService.default.download(id: id!, uri: uri, success: { (data) in
                self.onDataSet(data, uri.hash)
            }, progress: { (progress) in
                self.onProgressChanged(progress)
            }, error: { (error) in
                self.onError(error)
            })
        }
    }
    
    // on progress changed
    internal func onProgressChanged(_ progress: CGFloat) {
        
    }
    
    // on error
    internal func onError(_ error: DownloadStatus) {
        if error == .OK {
            return
        }
    }
    
    // prepare view for download
    internal func prepareForDownload() {
        self.imageView.alpha = 0
    }
    
    // on data fetched set the image
    internal func onDataSet(_ data: Data, _ hash: Int){
        if self.hashId != hash {
            return
        }
        self.imageView.alpha = 0
        // must assign the width and height in the main thread
        let width = bounds.width
        let height = bounds.height
        let currUri = self.urlString!
        DispatchQueue.global().async {
            if let image = UIImage(data: data) {
                if let image = resizeImage(image: image, width: width, height: height) {
                    DispatchQueue.main.async {
                        if currUri == self.urlString! {
                            self.onImageSet(image)
                        }
                    }
                }
            }
        }
    }
    
    // set image and remove loading
    internal func onImageSet(_ image: UIImage) {
        // set image
        self.imageView.image = image
        // animate the new image and hide the loading view
        UIView.animate(withDuration: 0.2, animations: {
            self.imageView.alpha = 1
        }) { (_) in
        }
    }
    
    // cancel prevoius subsctipton
    internal func cancelSubscription() {
        if let id = id, let url = urlString {
            CacheService.default.unsubscribe(uri: url, id: id)
        }
        // unset the id and the url
        id = nil
    }
    
    // add tap gesture
    internal func onTap() {
        if let viewController = viewController, let image = image {
            let picture = CollieGalleryPicture(image: image)
            let gallery = CollieGallery(pictures: [picture])
            gallery.presentInViewController(viewController)
        }
    }
    
}
