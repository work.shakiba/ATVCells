
import UIKit
import Foundation
import RxSwift

public class CacheService {
    
    static var `default` = CacheService()
    var images: [Int: Data] = [:]
    var downloads: [Int: DownloadObserver] = [:]
    
    public func getFile(_ uri: String) -> Data? {
        let hash = uri.hash
        return images[hash]
    }
    
    func unsubscribe(uri: String, id: String) {
        if let observer = downloads[uri.hash] {
            observer.unsubscribe(id)
        }
    }
    
    func download(id: String, uri: String, success: @escaping (Data) -> Void, progress: @escaping (CGFloat) -> Void, error: @escaping (DownloadStatus) -> Void) {
        if let observer = downloads[uri.hash] {
            let listener = DownloadSubscription()
            listener.download = { data in
                success(data)
            }
            listener.progress = { prog in
                progress(prog)
            }
            listener.error = { err in
                error(err)
            }
            observer.add(id, listener)
            return
        }
        let observer = DownloadObserver()
        self.downloads[uri.hash] = observer
        let listener = DownloadSubscription()
        listener.download = { data in
            success(data)
        }
        listener.progress = { prog in
            progress(prog)
        }
        listener.error = { err in
            error(err)
        }
        observer.add(id, listener)
        let session = DownloadSession(url: uri)
        session.fileHadnler = { data in
            observer.execute(with: data)
            self.onDataFetched(data, for: uri)
        }
        session.proggHandler = { progg in
            observer.progress(progg)
        }
        session.errorHandler = { err, meesage in
            observer.error(err)
            self.dispose(uri.hash)
        }
    }
    
    private func onDataFetched(_ data: Data, for url: String) {
        self.dispose(url.hash)
        if data.count > CacheDefaultSetting.default.maxImageSizeToCache { print("not save due to large size"); return }
        self.images[url.hash] = data
        if self.images.count > CacheDefaultSetting.default.maxCachedSize {
            for key in Array(self.images.keys)[0...10] {
                self.images.removeValue(forKey: key)
            }
        }
    }
    
    func dispose(_ hash: Int) {
        downloads[hash]?.dispose()
        downloads[hash] = nil
    }
    
}

public class CacheDefaultSetting {
    
    public static var `default` = CacheDefaultSetting()
    
    public var maxImageSizeToCache = 1000 * 1000 * 1
    public var maxCachedSize = 100
    
}





