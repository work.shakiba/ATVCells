//
//  DonwloadObserver.swift
//  Pods
//
//  Created by mohsen shakiba on 2/22/1396 AP.
//
//

import Foundation

class DownloadObserver {
    
    private var listeners: [String: DownloadSubscription] = [:]
    
    func execute(with data: Data) {
        for (_, listener) in listeners {
            listener.success(data)
        }
        dispose()
    }
    
    func progress(_ prog: CGFloat) {
        for (_, listener) in listeners {
            listener.progress?(prog)
        }
    }
    
    func error(_ err: DownloadStatus) {
        for (_, listener) in listeners {
            listener.error?(err)
        }
        dispose()
    }
    
    func unsubscribe(_ id: String) {
        listeners[id] = nil
    }
    
    func dispose() {
        for (_, listener) in listeners {
            listener.dispose()
        }
        listeners.removeAll()
    }
    
    func add(_ id: String, _ listener: DownloadSubscription) {
        listeners[id] = listener
    }
}
