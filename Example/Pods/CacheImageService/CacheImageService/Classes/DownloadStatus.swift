//
//  DownloadStatus.swift
//  Pods
//
//  Created by mohsen shakiba on 2/22/1396 AP.
//
//

import Foundation

public enum DownloadStatus {
    
    case OK
    case Failed
    case Error
    
}
