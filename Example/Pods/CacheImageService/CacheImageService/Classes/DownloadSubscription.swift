//
//  DownloadSubscription.swift
//  Pods
//
//  Created by mohsen shakiba on 2/22/1396 AP.
//
//

import Foundation

class DownloadSubscription {
    
    var id: String = String.randomString(length: 10)
    var download: ((Data) -> Void)?
    var progress: ((CGFloat) -> Void)?
    var error: ((DownloadStatus) -> Void)?
    
    func success(_ data: Data) {
        self.download?(data)
    }
    
    func dispose() {
        download = nil
        progress = nil
        error = nil
    }
    
}
