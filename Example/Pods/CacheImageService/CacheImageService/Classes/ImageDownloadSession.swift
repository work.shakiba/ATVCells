//
//  ImageDownloadSession.swift
//  Pods
//
//  Created by mohsen shakiba on 2/21/1396 AP.
//
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire

public class DownloadSession: NSObject, URLSessionDownloadDelegate {
    //    NSObject, URLSessionDownloadDelegate
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    var lastProgg: Float = 0
    
    var fileHadnler: ((Data) -> Void)?
    var proggHandler: ((CGFloat) -> Void)?
    var errorHandler: ((DownloadStatus, String?) -> Void)?
    
//    init(url: String) {
//        let utilityQueue = DispatchQueue.global(qos: .utility)
//        Alamofire.request(url)
//            .downloadProgress(queue: utilityQueue) { progress in
//                let progg = Float(progress.fractionCompleted)
//                if progg - self.lastProgg < 0.1 {
//                    return
//                }
//                self.lastProgg = progg
//                self.proggHandler?(CGFloat(progg))
//            }
//            .responseData { response in
//                if let data = response.result.value {
//                    self.fileHadnler?(data)
//                }
//                self.fileHadnler = nil
//                self.errorHandler = nil
//                self.proggHandler = nil
//        }
//    }
    
        init(url: String) {
            super.init()
            let config = URLSessionConfiguration.background(withIdentifier: url + String.randomString(length: 5))
            backgroundSession = Foundation.URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
            let url = URL(string: url)!
            downloadTask = backgroundSession.downloadTask(with: url)
            downloadTask.resume()
        }
    
        public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64){
            let progg = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
            if progg - lastProgg < 0.1 {
                return
            }
            lastProgg = progg
            self.proggHandler?(CGFloat(progg))
        }
    
        public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
            if let data = try? Data(contentsOf: location) {
                self.onDownloadFinish(data: data, error: nil)
            }else{
                self.onDownloadFinish(data: nil, error: "Date not available")
            }
        }
    
        public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?){
            if (error != nil) {
                self.onDownloadFinish(data: nil, error: error!.localizedDescription)
            }
            downloadTask = nil
    
        }
    
        func onDownloadFinish(data: Data?, error: String?) {
            if let data = data {
                self.fileHadnler?(data)
                self.errorHandler?(DownloadStatus.OK, nil)
            }else if let error = error {
                self.errorHandler?(DownloadStatus.Error, error)
            }else{
                self.errorHandler?(DownloadStatus.Error, nil)
            }
            self.fileHadnler = nil
            self.errorHandler = nil
            self.proggHandler = nil
        }
    
}






