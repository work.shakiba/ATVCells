//
//  ImageHelper.swift
//  Pods
//
//  Created by mohsen shakiba on 2/23/1396 AP.
//
//

import Foundation
import UIKit

func resizeImage(image: UIImage, newSize: CGSize) -> (UIImage) {
    
    let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
    let context = UIGraphicsGetCurrentContext()
    
    // Set the quality level to use when rescaling
    context!.interpolationQuality = .high
    let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
    
    context!.concatenate(flipVertical)
    // Draw into the context; this scales the image
    context?.draw(image.cgImage!, in: CGRect(x: 0.0,y: 0.0, width: newRect.width, height: newRect.height))
    
    let newImageRef = context!.makeImage()! as CGImage
    let newImage = UIImage(cgImage: newImageRef)
    
    // Get the resized image from the context and a UIImage
    UIGraphicsEndImageContext()
    
    return newImage
}

func resizeImage(image: UIImage, width:CGFloat, height :CGFloat) -> UIImage?
{
    let oldWidth = image.size.width;
    let oldHeight = image.size.height;
    
    let scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    let newHeight = oldHeight * scaleFactor;
    let newWidth = oldWidth * scaleFactor;
    let newSize = CGSize(width: newWidth, height: newHeight)
    
    return resizeImage(image: image, newSize: newSize);
}
