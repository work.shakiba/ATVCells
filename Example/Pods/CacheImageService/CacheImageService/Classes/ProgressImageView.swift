//
//  AdvancedImageView.swift
//  Pods
//
//  Created by mohsen shakiba on 2/21/1396 AP.
//
//

import UIKit
import CollieGallery

public class ProgressImageView: AdvancedImageView {
    
    // progress view
    var loadingView: CircularLoaderView!
    
    public var progress: CGFloat {
        set {
            self.loadingView.progress = newValue
        }
        get {
            return self.loadingView.progress
        }
    }
    
    // loading indicator
    public var loadingIndicatorColor: UIColor {
        set {
            self.loadingView.color = newValue
        }
        get {
            return self.loadingView.color
        }
    }
    
    // default init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    // default init
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        config()
    }
    
    // config the view
    internal override func config() {
        super.config()
        loadingView = CircularLoaderView(frame: frame)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(loadingView)
        let constraintLoadingTop = NSLayoutConstraint(item: loadingView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let constraintLoadingRight = NSLayoutConstraint(item: loadingView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)
        let constraintLoadingBottom = NSLayoutConstraint(item: loadingView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let constraintLoadingLeft = NSLayoutConstraint(item: loadingView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([constraintLoadingTop, constraintLoadingRight,constraintLoadingBottom,constraintLoadingLeft])
    }
    
    // on progress changed
    internal override func onProgressChanged(_ progress: CGFloat) {
        print("on progress changed", progress, loadingView.alpha)
        loadingView.progress = progress
    }
    
    // prepare view for download
    internal override func prepareForDownload() {
        super.prepareForDownload()
        loadingView.alpha = 1
        progress = 0
        self.loadingView.isHidden = false
    }
    
    // set image and remove loading
    internal override func onImageSet(_ image: UIImage) {
        // set image
        self.imageView.image = image
        // animate the new image and hide the loading view
        UIView.animate(withDuration: 0.25, animations: {
            self.imageView.alpha = 1
            self.loadingView.alpha = 0
        }) { (_) in
            self.loadingView.progress = 0
            self.loadingView.isHidden = true
        }
    }


}
