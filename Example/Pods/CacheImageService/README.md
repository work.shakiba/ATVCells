# CacheImageService

[![CI Status](http://img.shields.io/travis/mohsen shakiba/CacheImageService.svg?style=flat)](https://travis-ci.org/mohsen shakiba/CacheImageService)
[![Version](https://img.shields.io/cocoapods/v/CacheImageService.svg?style=flat)](http://cocoapods.org/pods/CacheImageService)
[![License](https://img.shields.io/cocoapods/l/CacheImageService.svg?style=flat)](http://cocoapods.org/pods/CacheImageService)
[![Platform](https://img.shields.io/cocoapods/p/CacheImageService.svg?style=flat)](http://cocoapods.org/pods/CacheImageService)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CacheImageService is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CacheImageService"
```

## Author

mohsen shakiba, work.shakiba@gmail.com

## License

CacheImageService is available under the MIT license. See the LICENSE file for more info.
