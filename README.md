# ATVCells

[![CI Status](http://img.shields.io/travis/mohsenShakiba/ATVCells.svg?style=flat)](https://travis-ci.org/mohsenShakiba/ATVCells)
[![Version](https://img.shields.io/cocoapods/v/ATVCells.svg?style=flat)](http://cocoapods.org/pods/ATVCells)
[![License](https://img.shields.io/cocoapods/l/ATVCells.svg?style=flat)](http://cocoapods.org/pods/ATVCells)
[![Platform](https://img.shields.io/cocoapods/p/ATVCells.svg?style=flat)](http://cocoapods.org/pods/ATVCells)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ATVCells is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ATVCells"
```

## Author

mohsenShakiba, work.shakiba@gmail.com

## License

ATVCells is available under the MIT license. See the LICENSE file for more info.
